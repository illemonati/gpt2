from keras_gpt_2 import load_trained_model_from_checkpoint, get_bpe_from_files, generate
import os
import tensorflow as tf
from flask import Flask, flash, redirect, render_template, request, session, abort

global graph
graph = tf.get_default_graph()

model_folder = 'gpt-2/models/774M'
config_path = os.path.join(model_folder, 'hparams.json')
checkpoint_path = os.path.join(model_folder, 'model.ckpt')
encoder_path = os.path.join(model_folder, 'encoder.json')
vocab_path = os.path.join(model_folder, 'vocab.bpe')


print('Load model from checkpoint...')
model = load_trained_model_from_checkpoint(config_path, checkpoint_path)
print('Load BPE from files...')
bpe = get_bpe_from_files(encoder_path, vocab_path)
print('Generate text...')


# If you are using the 117M model and top_k equals to 1, then the result will be:
# "From the day forth, my arm was broken, and I was in a state of pain. I was in a state of pain,"
def get_output(text, n):
    with graph.as_default():
        output = generate(model, bpe, [text], length=n, top_k=1)
        return output[0]

#
# def parse_line(line):
#     return line.split(" ")[0], int(line.split(" ")[1])
#
#
# def main():
#     try:
#         line = input('Enter "<text> <length>\n"')
#         parsed = parse_line(line)
#         print(get_output(*parsed))
#     except Exception as e:
#         print(e)
#

app = Flask(__name__)

@app.route("/")
def index():
    return render_template(
        'index.html'
    )

@app.route('/process',methods = ['POST', 'GET'])
def result():
    if request.method == 'POST':
        print(request.form)
        text = str(request.form['seed'])
        length = int(request.form['length'])
        print(text)
        print(length)
        result = get_output(text, length) if length < 10000 else "Put less then 10000!"
        return render_template("result.html",result = result)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=11191)
